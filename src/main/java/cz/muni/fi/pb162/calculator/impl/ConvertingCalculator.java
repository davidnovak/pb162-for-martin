/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.NumeralConverter;

/**
 *
 * @author david
 */
public interface ConvertingCalculator extends Calculator, NumeralConverter {
    
}
