/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Result;

/**
 *
 * @author david
 */
public class CalculationResult implements Result{

    /**
     * This field contain information if the calculation was successful or not.
     */
    protected final boolean success;
    
    /**
     * If successful and the result is numeric, this field contains the numeric value of the result. 
     *  Otherwise, this field contains Double.NaN.
     */
    protected final double numericValue;

    /**
     * If successful and the result is string, this field contains the string value; also, if 
     *  the calculation was NOT successful, this field contains the error message. Otherwise, it
     *  contains null.
     */
    protected final String alphanumericValue;

    /**
     * This protected constructor is used to fill all three fields of this class.
     * @param success
     * @param numericValue
     * @param alphanumericValue 
     */
    protected CalculationResult(boolean success, double numericValue, String alphanumericValue) {
        this.success = success;
        this.numericValue = numericValue;
        this.alphanumericValue = alphanumericValue;
    }
    
    /**
     * This constructor is to be used in the following two cases:
     * <ul>
     *   <li>the calculation was successful, but the result is alphanumeric</li>
     *   <li>or the calculation was NOT successful and then the second parameter contains the error message </li>
     * </ul>
     * @param success flag, if the calculation was successful
     * @param alphanumericValue the string result of the calculation or the error message
     */
    public CalculationResult(boolean success, String alphanumericValue) {
        this(success, Double.NaN, alphanumericValue);
    }

    
    public CalculationResult(boolean success, double numericValue) {
        this(success, numericValue, null);
    }
    
    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public boolean isAlphanumeric() {
        return alphanumericValue != null;
    }

    @Override
    public boolean isNumeric() {
        return numericValue != Double.NaN;
    }

    @Override
    public double getNumericValue() {
        return numericValue;
    }

    @Override
    public String getAlphanumericValue() {
        return alphanumericValue;
    }
    
}
